Hooks.on('setup', () => {
   
    if (game.modules.get('pf2e-tokens-characters')?.active) {
     
        fetch('modules/lang-pl-pf2e/translation/pf2e-tokens-characters/pl.json')
            .then(response => response.json())
            .then(translations => {
                game.i18n.translations = mergeObject(game.i18n.translations, translations);
                
            })
            .catch(err => console.error("Failed to load Polish translations to module pf2e-tokens-characters", err));
    }
});

Hooks.once("init", () => {
    if (typeof Babele !== "undefined") {
        game.settings.register("lang-pl-pf2e", "dual-language-names", {
            name: "Wyświetl nazwy po polsku i angielsku",
            hint: 'Oprócz nazwy polskiej wyświetlaj nazwę oryginalną (o ile się różni) poniżej dla wszystkich elementów typu "rzecz"',
            scope: "world",
            type: Boolean,
            default: true,
            config: true,
            onChange: foundry.utils.debounce(() => {
                window.location.reload();
            }, 100),
        });

        game.babele.register({
            module: "lang-pl-pf2e",
            lang: "pl",
            dir: "translation/pl/compendium",
        });

        game.babele.registerConverters({
            translateActorDescription: (data, translation) => {            
                return game.langPlPf2e.translateActorDescription(data, translation);
            },
            translateActorItems: (data, translation, dataObject, translatedCompendium, translationObject) => {
                return game.langPlPf2e.translateActorItems(
                    data,
                    translation,
                    dataObject,
                    translatedCompendium,
                    translationObject
                );
            },
        
            translateDuration: (data) => {
                return game.langPlPf2e.translateValue("duration", data);
            },
            translateHeightening: (data, translation) => {
                return game.langPlPf2e.dynamicObjectListMerge(
                    data,
                    translation,
                    game.langPlPf2e.getMapping("heightening", true)
                );
            },
            
            translateRules: (data, translation) => {
                return game.langPlPf2e.translateRules(data, translation);
            },
            translateSource: (data) => {
                return game.langPlPf2e.translateValue("source", data);
            },

            translateTime: (data) => {
                return game.langPlPf2e.translateValue("time", data);
            },
            translateTokenName: (data, translation, _dataObject, _translatedCompendium, translationObject) => {
                return game.langPlPf2e.translateTokenName(data, translation, translationObject);
            },
        });
    }
   
});

Hooks.on(`renderItemSheetPF2e`, (document, html) => {      
addoryginalname(document,html)    
});

Hooks.on(`renderNPCSheetPF2e`, (document, html) => {      
addoryginalname2(document,html)    
});

Hooks.on(`renderHazardSheetPF2e`, (document, html) => {      
addoryginalname3(document,html)    
});

Hooks.on(`renderVehicleSheetPF2e`, (document, html) => {      
addoryginalname4(document,html)    
});


function addoryginalname(document,html){      
    if (game.settings.get("lang-pl-pf2e", "dual-language-names")){
        if (document.document.flags.babele && document.document.flags.babele.originalName) {            
            const originalname = document.document.flags.babele.originalName;
            const translatedname = html[0].querySelector('input[name="name"]').value;
            const translatednamehtml = html[0].querySelector('input[name="name"]');            
            const action = html[0].querySelector('span.action-glyph');
            if ( originalname !== translatedname){           
                const detailElement = html[0].querySelector('.details');                     
                const levelElement = html[0].querySelector('.level');   
                const detailsDivhtml = translatednamehtml?.outerHTML;  
                const engnamehtml = `
                   <div style="flex:1;">       
                        ${detailsDivhtml}
                       <div class="english-name">
                           <span>${originalname}</span>     
                       </div>
                   </div>`;
                             
                translatednamehtml.remove();
                detailElement.insertAdjacentHTML(`afterbegin`, engnamehtml);
                if (levelElement){                    
                    action.remove();           
                    levelElement.insertAdjacentHTML(`beforeend`, action.outerHTML);
                }     
            }

        } else {
            console.log("Zmienna originalname nie istnieje lub ma wartość undefined, null lub pustą.");
        }
    }
}

function addoryginalname2(document,html){      
    if (game.settings.get("lang-pl-pf2e", "dual-language-names")){
        if (document.document.flags.babele && document.document.flags.babele.originalName) {
            
            const originalname = document.document.flags.babele.originalName;
            const translatedname = html[0].querySelector('input[name="name"]').value;
            if ( originalname !== translatedname){                
                const engnamehtml = `
                       <div class="english-name-other">
                           <span>${originalname}</span>     
                       </div>`;
                
                const detailsDiv3 = html[0]?.querySelector('.image-container');
       
                detailsDiv3.insertAdjacentHTML(`afterend`, engnamehtml);
            }
        } else {
            console.log("Zmienna originalname nie istnieje lub ma wartość undefined, null lub pustą.");
        }
    }
}

function addoryginalname3(document,html){      
    if (game.settings.get("lang-pl-pf2e", "dual-language-names")){
        if (document.document.flags.babele && document.document.flags.babele.originalName) {
            
            const originalname = document.document.flags.babele.originalName;
            const translatedname = html[0].querySelector('input[name="name"]').value;
            if ( originalname !== translatedname){                
                const engnamehtml = `
                       <section class="english-name-other">
                           <span>${originalname}</span>     
                       </section>`;
                
                const detailsDiv3 = html[0]?.querySelector('.sidebar');

                detailsDiv3.insertAdjacentHTML(`afterbegin`, engnamehtml);
            }
        } else {
            console.log("Zmienna originalname nie istnieje lub ma wartość undefined, null lub pustą.");
        }
    }
}

function addoryginalname4(document,html){      
    if (game.settings.get("lang-pl-pf2e", "dual-language-names")){
        if (document.document.flags.babele && document.document.flags.babele.originalName) {
            
            const originalname = document.document.flags.babele.originalName;
            const translatedname = html[0].querySelector('input[name="name"]').value;
            if ( originalname !== translatedname){                
                const engnamehtml = `
                       <div class="english-name-vehicle">
                           <span>${originalname}</span>     
                       </div>`;
                
                const detailsDiv3 = html[0]?.querySelector('.char-name');
                console.log(detailsDiv3);
                detailsDiv3.insertAdjacentHTML(`afterend`, engnamehtml);
            }
        } else {
            console.log("Zmienna originalname nie istnieje lub ma wartość undefined, null lub pustą.");
        }
    }
}
